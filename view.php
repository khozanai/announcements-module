<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");

$id = required_param('id', PARAM_INT);
$message = $DB->get_record('programforums', ['id' => $id], '*', MUST_EXIST);
$category = $DB->get_record('course_categories', array('id' => $message->categoryid), '*', MUST_EXIST);
$page_url = new moodle_url('/blocks/program_forums/view.php', ['id' => $message->id]);
if ($category) {
    $PAGE->set_category_by_id($category->id);
    $PAGE->set_url($page_url);
    $PAGE->set_pagetype('course-index-category');
    // And the object has been loaded for us no need for another DB call
    // $category = $PAGE->category;
} else {
    $id = 0;
    $PAGE->set_url('/course/index.php');
    $PAGE->set_context(context_system::instance());
}

$PAGE->set_pagelayout('coursecategory');
$PAGE->get_renderer('core', 'course');
$PAGE->set_title($category->name . ': ' . $message->title);
$PAGE->navbar->add($category->name, new moodle_url('/course/index.php', ['categoryid' => $category->id]));
$PAGE->navbar->add($category->name . ': ' . $message->title, $page_url);

$context = context_course::instance($COURSE->id);

echo $OUTPUT->header();

echo $OUTPUT->heading($message->title);
$br = html_writer::empty_tag('br');
$out = array();
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
foreach ($files as $file) {
    $filename = $file->get_filename();
    $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
    $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    $out[] = html_writer::link($url, $filename).$br . $br;
}
$post .= $message->message;
$post .= '<span style="font-weight: bold;">' . implode($out) . '</span>';

echo $post;


echo $OUTPUT->footer();





