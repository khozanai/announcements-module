<?php

/**
 * Class block_program_forums
 *
 * @package    block_program_forums
 * @author raymond mlambo
 * 
 */
//require_login_course($COURSE, true, $cm);
class block_program_forums extends block_base {

    public $query;
    public $result;

//require_login_course($COURSE, true, $cm);
    function init() {
        $this->title = get_string('pluginname', 'block_program_forums');
    }

    public function specialization() {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_program_forums');
            } else {
                $this->title = $this->config->title;
            }

            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_program_forums');
            }
        }
    }

    public function get_content() {
        global $CFG, $DB, $COURSE, $USER;

        $capabilities = array(
            'moodle/backup:backupcourse',
            'moodle/category:manage',
            'moodle/course:create',
            'moodle/site:approvecourse',
            'moodle/restore:restorecourse'
        );
        $context = context_course::instance($COURSE->id);
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return $this->content;
        }

        $this->content = new stdClass;

        $cat = $DB->get_record('course', array('id' => $COURSE->id));
        /*
         *  Count the number of the dean's messages
         */

        $deanMessages = $DB->get_records('dean_messages');
        $counter = 0;
        foreach ($deanMessages as $deanMessage) {
            if (!empty($deanMessage->id)) {
                $counter++;

                $unreadMessages = $DB->get_records('dean_messages_views', array(
                    'messageid' => $deanMessage->id,
                    'userid' => $USER->id
                ));
                foreach ($unreadMessages as $unreadMessage) {
                    if (!empty($unreadMessage->id)) {
                        $counter = $counter - 1;
                    }
                }
            }
        }
        /**
         * Check the count of unread forums for this user
         */
        $programmeForums = $DB->get_records('programforums', array('categoryid' => $cat->category));
        $count = 0;
        foreach ($programmeForums as $programmeForum) {
            if (!empty($programmeForum->id)) {
                $count++;

                $unreadForums = $DB->get_records('programforums_views', array(
                    'programforumid' => $programmeForum->id,
                    'userid' => $USER->id
                ));
                foreach ($unreadForums as $unreadForum) {
                    // foreach entry you find, mark it as read, and remove it from the count variable
                    if (!empty($unreadForum->id)) {
                        $count = $count - 1;
                    }
                }
            }
        }
        /**
         * Check the count of the unread posts from the course announcements
         */
        $courseAnnouncements = $DB->get_records('course_forums', array('courseid' => $COURSE->id));
        $courseAnnouncementsCount = 0;
        foreach ($courseAnnouncements as $courseAnnouncement) {
            if (!empty($courseAnnouncement->id)) {
                $courseAnnouncementsCount++;

                $unReadCourseMessages = $DB->get_records('course_forums_views', array(
                    'courseforumid' => $courseAnnouncement->id,
                    'userid' => $USER->id
                ));

                foreach ($unReadCourseMessages as $unReadCourseMessage) {
                    if (!empty($unReadCourseMessage->id)) {
                        $courseAnnouncementsCount = $courseAnnouncementsCount - 1;
                    }
                }
            }
        }
        /**
         * check the count of discussions from this course, for this user
         */
        $countViewedDiscussions = 0;
        $courseDiscussions = $DB->get_records_sql("select id from {course_discussions} where courseid = ?", array($COURSE->id));

        foreach ($courseDiscussions as $courseDiscussion) {
            if (!empty($courseDiscussion->id)) {
                $countViewedDiscussions++;

                $viewedDiscussions = $DB->get_records('course_discussions_views', array(
                    'coursediscussionid' => $courseDiscussion->id,
                    'userid' => $USER->id
                ));
                foreach ($viewedDiscussions as $viewedDiscussion) {
                    if (!empty($viewedDiscussion->id)) {
                        $countViewedDiscussions = $countViewedDiscussions - 1;
                    }
                }
            }
        }


        // prep the url for the dean's messages
        $deanURL = new moodle_url('/admin/regenesysdean/', array(
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
            'userid' => $USER->id
        ));

        // prep the URL for discussions announcements
        $discussionurl = new moodle_url('/course/discussions/index.php', ['blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
            'userid' => $USER->id
        ]);

        $this->content->text .= '<div style="margin: 1%; border: 1px solid #CCC; padding-left: 8px; padding-right: 4px; padding-bottom: 4px; overflow-x: hidden;">';
        // The URL to the messages from the dean
        $dean_messages_url = new moodle_url('/blocks/program_forums/deanmessages.php', ['id' => $this->page->course->id, 'userid' => $USER->id]);
        $this->content->text .= html_writer::link($dean_messages_url, get_string('deanURL', 'block_program_forums'), array(
                    'id' => 'deanURL'
        ));
        $this->content->text .= '<br />';
        $this->content->text .= '&nbsp;&nbsp;&nbsp;<span style="font-size: 12px; font-family: FontAwesome;" >'
                . '' . $counter . '&nbsp;unread posts <i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span><br />';

        // The URL to the programmes
        $programmeURL = new moodle_url('/regenesys/programmeforums/index.php', array(
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
            'cat' => $cat->category
        ));
        $this->content->text .= html_writer::link($programmeURL, get_string('programmeURL', 'block_program_forums'), array(
                    'id' => 'programmeURL'
        ));
        $this->content->text .= '<br />';
        $this->content->text .= '&nbsp;&nbsp;&nbsp;<span style="font-size: 12px; font-family: FontAwesome;" >'
                . '' . $count . ' unread posts <i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span><br />';

        // display this if the user is on a course page only
        // Course announcements
        if ($COURSE->id > 1) {
            // prep the URL for course announcements
//            $courseURL = new moodle_url('/course/forums/index.php', array(
//                'blockid' => $this->instance->id,
//                'courseid' => $COURSE->id,
//                'userid' => $USER->id
//            ));
            $courseforums_url = new moodle_url('/blocks/program_forums/courseforums.php', ['id' => $this->page->course->id]);
            // course announcements
            $this->content->text .= '<a href="' . $courseforums_url . '">'
                    . '<span style="font-weight: bold;">Course announcements</span></a>';

            $this->content->text .= '<br />';
            $this->content->text .= '&nbsp;&nbsp;&nbsp;<span style="font-size: 12px; font-family: FontAwesome;" >'
                    . '' . $courseAnnouncementsCount . ' unread posts <i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span><br /';

            // discussions
            $this->content->text .= '<a href="' . $CFG->wwwroot . '/course/discussions/index.php?blockid=' . $this->instance->id . '&courseid=' . $COURSE->id . '&userid=' . $USER->id . '">'
                    . '<span style="font-weight: bold;">&nbsp;</span></a>';
            $this->content->text .= '<a href="' . $CFG->wwwroot . '/course/discussions/index.php?blockid=' . $this->instance->id . '&courseid=' . $COURSE->id . '&userid=' . $USER->id . '">'
                    . '<span style="font-weight: bold;">Discussions</span></a>';
            $this->content->text .= '<br />';
            $this->content->text .= '&nbsp;&nbsp;&nbsp;<span style="font-size: 12px; font-family: FontAwesome;" >'
                    . '' . $countViewedDiscussions . ' unread posts <i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></span><br />';
        }

        $createprgurl = new moodle_url('/regenesys/programmeforums/create.php'); // New Program Forums
        $addnewdiscussion = new moodle_url('/course/discussions/add.php', array(
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id)); // New Module Forums

        $manageurl = new moodle_url('/regenesys/moduleforums/manage.php', array(
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id));
        $this->content->text .= '</div>';
        // new dean message
        $new_message = new moodle_url('/admin/regenesysdean/newmessage.php', array(
            'blockid' => $this->instance->id,
            'userid' => $USER->id));

        $new_courseforum = new moodle_url('/course/forums/create.php', array(
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
            'userid' => $USER->id));
        if ($hassiteconfig or has_any_capability($capabilities, $context)) {

            //NEW COURSE FORUM LINK
            $newforum_url = new moodle_url('/blocks/program_forums/index.php', array(
                'id' => $this->page->course->id
            ));
            $new_programme_url = new moodle_url('/blocks/program_forums/programme_forum.php', array(
                'id' => $this->page->course->id
            ));
            // Message from the dean - the url and the link output
            $dean_message_url = new moodle_url('/blocks/program_forums/new_deanmessage.php', ['id' => $this->page->course->id, 'userid' => $USER->id]);
            $this->content->text .= '<i class="fa fa-plus-circle" arisa-hidden="true"></i> ';
            $this->content->text .= html_writer::link($dean_message_url, 'New Dean\'s message', array(
                        'id' => 'dean_message_url'
            ));
            $this->content->text .= html_writer::empty_tag('br');
            $this->content->text .= '<i class="fa fa-plus-circle" arisa-hidden="true"></i> ';
            $this->content->text .= html_writer::link($new_programme_url, 'New programme forum');
            $this->content->text .= '<br />';

            if ($COURSE->id > 1) {
                $this->content->text .= '<i class="fa fa-plus-circle" arisa-hidden="true"></i> ';
                $this->content->text .= html_writer::link($newforum_url, 'New course forum');
                $this->content->text .= '<br />';

                // Send message based on a schedule/ schedules
                $schedules_url = new moodle_url('/blocks/program_forums/schedules_form.php', ['id' => $this->page->course->id]);
                $this->content->text .= '<i class="fa fa-plus-circle" arisa-hidden="true"></i> ';
                $this->content->text .= html_writer::link($schedules_url, 'Send mail via schedules');
                $this->content->text .= '<br />';
            }
            // $this->content->footer .= html_writer::link($manageurl, get_string('manageforums', 'block_program_forums'));
        }



        return $this->content;
    }

    public function instance_allow_multiple() {
        return true;
    }

}
