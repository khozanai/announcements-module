<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * This is the string file for the plugin: Regenesys Course announcements
 */

$string['pluginname'] = 'Announcements';
$string['program_forums'] = 'Programme Forums';
$string['defaulttext'] = 'Regenesys';
$string['defaulttitle'] = 'Announcements';
$string['blockstring'] = 'Regenesys Business School';
$string['headerconfig'] = 'Programme Forums Settings';
$string['descconfig'] = 'Regenesys Desc Settings';
$string['labelallowhtml'] = 'Regenesys Allow HTML';
$string['courseforums'] = 'Module Forums';
$string['programmeURL'] = 'Programme&nbsp;news';
$string['courseURL'] = 'Course&nbsp;news';
$string['discussionurl'] = 'Discussions';
$string['deanURL'] = 'From the Dean\'s Office';
$string['createprogramforum'] = 'New programme forum';
$string['new_message'] = 'New Dean\'s Office message';
$string['newdiscussion'] = 'New discussion';
$string['new_courseforum'] = 'New course forum';
$string['manageforums'] = 'Manage Programme Forums';
$string['descallowhtml'] = 'Programme Forums Allow HTML Settings';
$string['regenesys:addinstance'] = 'Add a Programme Forums block';
$string['regenesys:myaddinstance'] = 'Add a new Regenesys Programme Forums block to the my moodle page';
