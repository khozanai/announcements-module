<?php

require_once '../../config.php';
$categoryid = required_param('categoryid', PARAM_INT);

$category = $DB->get_record('course_categories', array('id' => $categoryid), '*', MUST_EXIST);
$page_url = new moodle_url('/blocks/program_forums/view_programme_forums.php', ['categoryid' => $category->id]);
if ($category) {
    $PAGE->set_category_by_id($category->id);
    $PAGE->set_url($page_url);
    $PAGE->set_pagetype('course-index-category');
    // And the object has been loaded for us no need for another DB call
    // $category = $PAGE->category;
} else {
    $id = 0;
    $PAGE->set_url('/course/index.php');
    $PAGE->set_context(context_system::instance());
}
$PAGE->set_pagelayout('coursecategory');
$PAGE->get_renderer('core', 'course');
$PAGE->set_title($category->name);

$PAGE->navbar->add($category->name, new moodle_url('/course/index.php', ['categoryid' => $category->id]));
$PAGE->navbar->add('Messages', $page_url);
require_login();
echo $OUTPUT->header();

$br = html_writer::empty_tag('br');
// $messages = $DB->get_records('programforums', ['categoryid' => $category->id]);
$messages = $DB->get_records_sql("SELECT * FROM {programforums} WHERE categoryid = ? ORDER BY ID DESC", [$category->id]);

$table = new html_table();
$table->head = array('Message', 'Date');
foreach ($messages as $message) {
    $message_url = new moodle_url('/blocks/program_forums/view.php', ['id' => $message->id]);
    $row = new html_table_row([
        html_writer::link($message_url, $message->title),
        date('d-F-Y H:i:s:A', $message->timecreated)
            ]
    );
    $row->attributes['class'] = '';
    $table->data[] = $row;
}
echo html_writer::table($table);
echo $OUTPUT->footer();
