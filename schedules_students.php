<?php

/**
 *
 * @author Raymond Mlambo 2017
 * 
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");

$br = html_writer::empty_tag('br');
$schedules = optional_param('schedules', 0, PARAM_ALPHANUMEXT);

$table = new html_table();
$table->head = ['Count', 'Schedule', 'Firstname', 'Lastname', 'Email', 'Reggie #'];
$count = 0;
foreach ($schedules as $schedule) {
    $students_url = 'https://prodservices.regenesys.net/api/Moodle/GetStudentDetailsByScheduleID?Schedule_Id='
            . '' . $schedule . '&UserName=moodleadmin&Password=a9m1n';
    $outputs = json_decode(file_get_contents($students_url));
    foreach ($outputs as $output) {
        $count++;
        $row = new html_table_row([
            $count,
            '',
            $output->FirstName,
            $output->LastName,
            $output->Email_Address,
            $output->Student_Code
        ]);
        $table->data[] = $row;
    }
}

echo html_writer::table($table);
