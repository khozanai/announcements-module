<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT); // The Course ID

$course = get_course($id);
$context = context_course::instance($course->id);
$page_url = new moodle_url('/blocks/program_forums/courseforums.php', ['id' => $course->id]);
require_login($course);

$PAGE->set_context($context);
$PAGE->set_url($page_url);
$PAGE->set_title('Course forums: ' . $course->shortname);
if ($course->id < 2) {
    $PAGE->navbar->add('Courses', '/');
}
$PAGE->navbar->add($course->fullname . ': Course Forums', $page_url);
echo '<link rel="stylesheet" type="text/css" href="styles.css" />';
echo $OUTPUT->header();
$br = html_writer::empty_tag('br');
echo html_writer::start_tag('div', ['id' => 'container']);

echo '<span>
        <p style="font-size: 14px; font-weight: bold; line-height: 16px;">Keep an eye on this forum, where your facilitator will from time to time post messages, notes and other items of interest to guide you through this course.</p>
        <div style="text-indent: 20px;">You will see the posts both here and in your e-mail.</div>
        <div style="text-indent: 20px;"> To join the discussion,  click on the title of the relevant thread on this page, and post your answers, observations and queries there.</div>
        <div style="text-indent: 20px;">  Be aware that if you reply to the e-mail, your message is not posted to the forum, but goes only to the facilitator who posted the thread.</div><br />
        <p style="font-size: 14px; text-align: center; font-weight: bold; color: #008554;">   <a href="https://dev.regenesys.net/pluginfile.php/1359/mod_book/chapter/2194/Online%20forums%20acceptable%20use%20policy.pdf">Online Forums Acceptable Use Policy</a></p>           
    </span>';
echo $br;
$course_forums = $DB->get_records_sql("SELECT * FROM {course_forums} WHERE courseid = ? ORDER BY id DESC", [$course->id]);

$table = new html_table();
$table->head = ['Subject', 'Posted by', 'Date'];
foreach ($course_forums as $course_forum) {
    $user = $DB->get_record('user', ['id' => $course_forum->createdby]);
    $url = new moodle_url('/blocks/program_forums/view_courseforum.php', ['id' => $course_forum->id]);
    $row = new html_table_row([
        html_writer::link($url, $course_forum->subject),
        html_writer::link($url, fullname($user)),
        date('d-F-Y H:i:s:A', $course_forum->timecreated)
    ]);

    $table->data[] = $row;
}

echo html_writer::table($table);
echo html_writer::end_tag('div');
echo $OUTPUT->footer();
