<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");

$id = required_param('id', PARAM_INT);
$messageid = required_param('message', PARAM_INT);

$course = get_course($id);
$message = $DB->get_record('course_forums', ['id' => $messageid], '*', MUST_EXIST);

$url = new moodle_url('/blocks/program_forums/index.php?id=' . $course->id);
$context = context_course::instance($course->id);
$PAGE->set_context($context);
$PAGE->set_title('New: ' . $course->shortname . ' announcement');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname . ': New course announcement'), $url);
require_login($course);
$PAGE->requires->js('/blocks/program_forums/program_forums.js');
echo $OUTPUT->header();
$codes = [
    ['id' => 3, 'code' => 'MBA-9'],
    ['id' => 42, 'code' => 'MBA-8'],
    ['id' => 4, 'code' => 'HCBM'],
    ['id' => 8, 'code' => 'BBA'],
    ['id' => 9, 'code' => 'BBA'],
    ['id' => 69, 'code' => 'BBA'],
    ['id' => 23, 'code' => 'BPM'],
    ['id' => 24, 'code' => 'BPM'],
    ['id' => 25, 'code' => 'BPM'],
    ['id' => 27, 'code' => 'MPM'],
    ['id' => 44, 'code' => 'MPM'],
    ['id' => 19, 'code' => 'PDBM'],
    ['id' => 21, 'code' => 'HCPM'],
    ['id' => 26, 'code' => 'PDPM'],
    ['id' => 12, 'code' => 'BBA Bank'],
    ['id' => 13, 'code' => 'BBA Bank'],
    ['id' => 14, 'code' => 'BBA Bank'],
    ['id' => 16, 'code' => 'BBA-Retail'],
    ['id' => 17, 'code' => 'BBA-Retail'],
    ['id' => 18, 'code' => 'BBA-Retail']
];
$br = html_writer::empty_tag('br');
echo '<hr>';
echo $OUTPUT->heading($message->subject);

$out = array();
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
foreach ($files as $file) {
    $filename = $file->get_filename();
    $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
    $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    $out[] = html_writer::link($url, $filename) . $br;
}

$post .= $message->forum_message;
$post .= '<span style="font-weight: bold;">' . implode($out) . $br . '</span>';
echo $post;
echo '<hr>';

echo '<span id="' . $message->id . '" class="messageid"></span>';


echo $OUTPUT->heading("Select schedules below:");

// fetch the schedules from Reggie
echo $OUTPUT->heading("2017 Schedules");
foreach ($codes as $code) {
    if ($course->category == $code['id']) {
        $url = 'https://prodservices.regenesys.net/api/Moodle/GetScheduleDetailDetailsByQualCodeMoodleIdAndAcademicYear?'
                . 'qualCode=' . $code['code'] . '&mooodleID=' . $course->id . '&academicYear=2017&UserName=moodleadmin&Password=a9m1n';
        $results = json_decode(file_get_contents($url));
        foreach ($results as $result) {
            echo '<input type="checkbox" value="' . $result->Qualification_Schedule_Id . '" name="radios">: ' . $result->Schedule_Name . $br;
        }
    }
}

echo $OUTPUT->heading("2018 Schedules");
foreach ($codes as $code) {
    if ($course->category == $code['id']) {
        $url = 'https://prodservices.regenesys.net/api/Moodle/GetScheduleDetailDetailsByQualCodeMoodleIdAndAcademicYear?'
                . 'qualCode=' . $code['code'] . '&mooodleID=' . $course->id . '&academicYear=2018&UserName=moodleadmin&Password=a9m1n';
        $results = json_decode(file_get_contents($url));
        foreach ($results as $result) {
            echo '<input type="checkbox" value="' . $result->Qualification_Schedule_Id . '" name="radios">: ' . $result->Schedule_Name . $br;
        }
    }
}
echo $br;
echo '<span id="data"></span>' . $br;
echo '<button style="display: none;" id="send_schedule_message">Send message</button>';
echo '<span id="test_data"></span>';

/**
 * (
  [Qualificationid] => 4
  [QualificationName] => Higher Certificate in Business Management
  [Qualification_Schedule_Id] => 10687
  [Schedule_Name] => Online  2017
  [AcademicYear] => 2017
  [FirstName] =>
  [LastName] =>
  [MiddleName] =>
  [EmailAddress] =>
  [IDNumber] =>
  [Student_Code] =>
  [ModuleName] => Fundamentals of Business Management
  [QualCode] => HCBM
  [FinalAssignMark] => 0
  [FinalExamMark] => 0
  [FinalDigitalAssessment] => 0
  [FinalGroupWork] => 0
  [Result] =>
  [Grade] =>
  [OverAllMark] => 19
  [FinalMarkId] =>
  [AssignmentId] =>
  [ExamId] =>
  [ResultId] =>
  [ModuleId] => 0
  [IsExempted] =>
  [PortalCategory] =>
  )
 */
echo $OUTPUT->footer();
