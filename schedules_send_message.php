<?php

/**
 * @author Raymond Mlambo 2017
 * 
 * Send the message to the students
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");

$messageid = required_param('messageid', PARAM_INT);
$schedules = required_param('schedules', PARAM_ALPHANUMEXT);

$message = $DB->get_record('course_forums', ['id' => $messageid], '*', MUST_EXIST);

$out = array();
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
foreach ($files as $file) {
    $filename = $file->get_filename();
    $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
    $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    $out[] = html_writer::link($url, $filename) . $br;
}

$post .= $message->forum_message;
$post .= '<span style="font-weight: bold;">' . implode($out) . $br . '</span>';


//Prepare the data for e-mailing
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// Additional headers
$headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
$headers .= 'Cc: noreply@regenesys.co.za' . "\r\n";

foreach ($schedules as $schedule) {
    $students_url = 'https://prodservices.regenesys.net/api/Moodle/GetStudentDetailsByScheduleID?Schedule_Id='
            . '' . $schedule . '&UserName=moodleadmin&Password=a9m1n';
    $outputs = json_decode(file_get_contents($students_url));
    foreach ($outputs as $output) {
        mail($output->Email_Address, $message->subject, $post, $headers);
    }
}

// Send a copy of the e-mail
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// Additional headers
$admin = core_user::get_noreply_user();
$headers .= 'From: ' . fullname($admin) . '<' . $admin->email . '>' . "\r\n";
$headers .= 'Cc: raymondm@regenesys.co.za' . "\r\n";
mail($USER->email, $message->subject, $post, $headers);













