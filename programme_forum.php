<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */
require_once '../../config.php';
require_once 'lib.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once '../../course/forums/courseforums.php';
require_once($CFG->libdir . '/coursecatlib.php');
$id = required_param('id', PARAM_INT);

$course = get_course($id);

$url = new moodle_url('/blocks/program_forums/programme_forum.php?id=' . $course->id);
$context = context_course::instance($course->id);
$PAGE->set_context($context);
$PAGE->set_title('New: ' . $course->shortname . ' announcement');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
if ($course->id <= 2) {
    $PAGE->navbar->add('Courses', new moodle_url('/course'));
    // $PAGE->navbar->add('Programme courses', new moodle_url('/course/index.php?categoryid='.$course->id));
}
$PAGE->navbar->add('Programme courses', new moodle_url('/course/index.php?categoryid=' . $course->id));
$PAGE->navbar->add(('New Programme Announcement'), $url);
require_login($course);

class my_programme_forum_form extends moodleform {

    public function coursecategories() {
        global $DB;
        $courses = [];
        $coursecategories = $DB->get_records_sql("SELECT cc.id, cc.name FROM {course_categories} cc JOIN {course} c ON c.category = cc.id"
                . " WHERE cc.id > ?", [2]);
        foreach ($coursecategories as $coursecategory) {
            // $courses['id'] = $coursecategory->id;
            // $courses['name'] = $coursecategory->name;
            array_push($courses, $coursecategory->name);
        }
        return $courses;
    }

    public function definition() {
        global $CFG, $DB, $cm, $course;

        $uniqueid = uniqid();
        $mform = $this->_form; // Don't forget the underscore!  592ec946c28b3 592ec964bc3d3

        $mform->addElement('header', 'general', 'Programme forum message');
        // Adding the standard "name" field.
        $mform->addElement('text', 'subject', 'Subject', array('size' => '64'));

        $mform->addRule('subject', null, 'required', null, 'client');
        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        // $mform->addElement('select', 'category', get_string('forumtype', 'forum'), $this->coursecategories());
        $displaylist = coursecat::make_categories_list('moodle/backup:backupcourse');
        $mform->addElement('select', 'category', get_string('coursecategory'), $displaylist);
        $mform->addRule('category', null, 'required', null, 'client');

        $mform->addElement('editor', 'message', 'Message', null, array(
            'subdirs' => 1,
            'maxbytes' => 0,
            // 'maxfiles' => 1,
            'height' => 500, // height of the popup window
            'width' => 450,
            'changeformat' => 0,
            'context' => null,
            'noclean' => 0,
            'trusttext' => 0,
            'enable_filemanagement' => true));
        //);
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required', null, 'client');
        $mform->addElement('hidden', 'id', $course->id);

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('audio', 'video', 'document', '.xls', '.xlsx', '.ppt', '.pptx', '.pptp');
        $filemanager_options['maxbytes'] = 0;
        $filemanager_options['maxfiles'] = -1;
        $filemanager_options['mainfile'] = true;

        $mform->addElement('filemanager', 'files', 'Attach one or more files', null, $filemanager_options);


        $this->add_action_buttons();
    }

}

$mform = new my_programme_forum_form(null, array('id' => $course->id), PARAM_RAW);
$urltogo = new moodle_url('/course/view.php', array('id' => $course->id));
if ($mform->is_cancelled()) {
    purge_all_caches();
    redirect($urltogo);
} else if ($fromform = $mform->get_data()) {
    // Process form data
    $draftareaid = file_get_submitted_draft_itemid('message');
    $content = file_prepare_draft_area($draftareaid, $PAGE->context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message);

    $format = editors_get_preferred_format();

    $fs = get_file_storage();

    $content = file_save_draft_area_files($fromform->message['itemid'], $PAGE->context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message['text']);

    $record = new stdClass();
    $record->categoryid = $fromform->category;
    $record->title = $fromform->subject;
    $record->message = $fromform->message['text'];
    $record->timecreated = time();
    $record->createdby = $USER->id;
    $record->modifiedby = $USER->id;

    if ($save = $DB->insert_record('programforums', $record)) {
        file_save_draft_area_files($fromform->files, $context->id, 'block_program_forums', 'message', $save, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 50));

        // fetch the file and send it in the e-mail
        $br = html_writer::empty_tag('br');
        $out = array();
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $save, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $save . '/' . $filename;
            $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
            $out[] = html_writer::link($url, $filename) . $br;
        }
        $message .= $record->message;
        $message .= '<span style="font-weight: bold;">' . implode($out) . $br . '</span>';

        send_email_to_students($record->categoryid, $record->title, $message);
        redirect($urltogo);
    } else {
        throw new RuntimeException("Something went wrong");
    }
}
echo $OUTPUT->header();


$mform->display();


echo $OUTPUT->footer();
