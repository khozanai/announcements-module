<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('forumid', PARAM_INT); // Course forum message id

$forum = $DB->get_record('course_forums', ['id' => $id]);
$course = get_course($forum->courseid);
$context = context_course::instance($course->id);
$page_url = new moodle_url('/blocks/program_forums/courseforum_reply.php', ['id' => $forum->id]);
require_login($course);

// class for the form
class courseforum_reply_form extends moodleform {

    public static function file_options() {
        global $COURSE, $PAGE, $CFG;

        $configmaxbytes = $CFG->maxbytes;
        // $configmaxfiles = get_config('local_mail', 'maxfiles');
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes, $configmaxbytes);
        $maxfiles = is_numeric($configmaxfiles);
        return array('accepted_types' => '*',
            'maxbytes' => $maxbytes,
            'maxfiles' => $maxfiles,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => false);
    }

    public function definition() {
        global $DB, $CFG, $forum, $blockinstance;

        $mform = $this->_form;

        $mform->addElement('header', 'general', 'Forum message reply');

        $mform->addElement('editor', 'message', 'Message', null, array(
            'subdirs' => 1,
            'maxbytes' => 0,
            // 'maxfiles' => 1,
            'height' => 500, // height of the popup window
            'width' => 450,
            'changeformat' => 0,
            'context' => null,
            'noclean' => 0,
            'trusttext' => 0,
            'enable_filemanagement' => true));
        //);
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required', null, 'client');
        $mform->addElement('hidden', 'forumid', $forum->id);

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('audio', 'video', 'document', '.xls', '.xlsx', '.ppt', '.pptx', '.pptp');
        $filemanager_options['maxbytes'] = 0;
        $filemanager_options['maxfiles'] = -1;
        $filemanager_options['mainfile'] = true;

        $mform->addElement('filemanager', 'files', 'Attach one or more files', null, $filemanager_options);

        $this->add_action_buttons();
    }

}

$mform = new courseforum_reply_form(null, array('forumid' => $forum->id), PARAM_RAW);
$redirect_url = new moodle_url('/blocks/program_forums/view_courseforum.php', ['id' => $forum->id]);
if ($mform->is_cancelled()) {
    purge_all_caches();
    redirect($redirect_url);
} else if ($fromform = $mform->get_data()) {
    // Process form data
    $draftareaid = file_get_submitted_draft_itemid('message');
    $content = file_prepare_draft_area($draftareaid, $PAGE->context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message);

    // $format = editors_get_preferred_format();

    $fs = get_file_storage();

    $content = file_save_draft_area_files($fromform->message['itemid'], $context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message['text']);

    $record = new stdClass();
    $record->courseforumid = $fromform->forumid;
    $record->userid = $USER->id;
    $record->response = $fromform->message['text'];
    $record->timecreated = time();

    if ($save = $DB->insert_record('block_courseforums_replies', $record)) {
        // Save any files that may have been uploaded
        file_save_draft_area_files($fromform->files, $context->id, 'block_program_forums', 'message', $save, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 50));

        $reply = $DB->get_record('block_courseforums_replies', ['id' => $save]);
        $facilitator = $DB->get_record('user', ['id' => $forum->createdby]);
        $subject = 'New response to: ' . $forum->subject . ' by ' . $USER->firstname;
        // fetch the file and send it in the e-mail
        $br = html_writer::empty_tag('br');
        $out = array();
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $reply->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $reply->id . '/' . $filename;
            $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
            $out[] = html_writer::link($url, $filename) . $br;
        }
        $post .= $reply->response;
        $post .= '<span style="font-weight: bold;">' . implode($out) . $br . '</span>';

        // SEND THE EMAIL to the sender
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // Additional headers
        $headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
        $headers .= 'Cc: raymondm@regenesys.co.za' . "\r\n";
        $headers .= 'Cc: noreply@regenesys.co.za' . "\r\n";
        mail($facilitator->email, $subject, $post, $headers);

        redirect($redirect_url);
    } else {
        throw new RuntimeException("Something went wrong");
    }
}

$PAGE->set_context($context);
$PAGE->set_url($page_url);
$PAGE->set_title('Course forum: ' . $forum->subject);
if ($course->id < 2) {
    $PAGE->navbar->add('Courses', '/');
}
$PAGE->navbar->add($course->fullname . ': Course Forums', 'courseforums.php?id=' . $course->id);
$PAGE->navbar->add($forum->subject, $page_url);

echo $OUTPUT->header();
echo $mform->display();
echo $OUTPUT->footer();





