<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);

$course = get_course($id);

$url = new moodle_url('/blocks/program_forums/new_deanmessage.php', ['id' => $course->id, 'userid' => $USER->id]);
$context = context_course::instance($course->id);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_title('Dean\'s message');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname), new moodle_url('/'));
$PAGE->navbar->add(('New Message From The Dean'), $url);

class deanmessage_form extends moodleform {

    public static function file_options() {
        global $COURSE, $PAGE, $CFG;

        $configmaxbytes = $CFG->maxbytes;
        // $configmaxfiles = get_config('local_mail', 'maxfiles');
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes, $configmaxbytes);
        $maxfiles = is_numeric($configmaxfiles);
        return array('accepted_types' => '*',
            'maxbytes' => $maxbytes,
            'maxfiles' => $maxfiles,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => false);
    }

    public function definition() {
        global $CFG, $USER, $course, $blockinstance;
        $uniqueid = uniqid();
        $mform = $this->_form; // Don't forget the underscore!  592ec946c28b3 592ec964bc3d3

        $mform->addElement('header', 'general', 'Dean\'s message');
        // Adding the standard "name" field.
        $mform->addElement('text', 'subject', 'Subject', array('size' => '64'));

        $mform->addRule('subject', null, 'required', null, 'client');
        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('editor', 'message', 'Message', null, array(
            'subdirs' => 1,
            'maxbytes' => 0,
            // 'maxfiles' => 1,
            'height' => 500, // height of the popup window
            'width' => 450,
            'changeformat' => 0,
            'context' => null,
            'noclean' => 0,
            'trusttext' => 0,
            'enable_filemanagement' => true));
        //);
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required', null, 'client');
        $mform->addElement('hidden', 'id', $course->id);
        $mform->addElement('hidden', 'userid', $USER->id);

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('audio', 'video', 'document', '.xls', '.xlsx', '.ppt', '.pptx', '.pptp');
        $filemanager_options['maxbytes'] = 0;
        $filemanager_options['maxfiles'] = -1;
        $filemanager_options['mainfile'] = true;

        $mform->addElement('filemanager', 'files', 'Attach one or more files', null, $filemanager_options);


        $this->add_action_buttons();
    }

    function validation($data, $files) {
        global $USER;
        if ($data) {
            $errors = parent::validation($data, $files);

            $usercontext = context_user::instance($USER->id);
            $fs = get_file_storage();
//            if (!$files = $fs->get_area_files($usercontext->id, 'user', 'draft', $data['files'], 'sortorder, id', false)) {
//                $errors['files'] = get_string('required');
//                return $errors;
//            }
            if (count($files) == 1) {
                // no need to select main file if only one picked
                return $errors;
            } else if (count($files) > 1) {
                $mainfile = false;
                foreach ($files as $file) {
                    if ($file->get_sortorder() == 1) {
                        $mainfile = true;
                        break;
                    }
                }
                // set a default main file
                if (!$mainfile) {
                    $file = reset($files);
                    file_set_sortorder($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), 1);
                }
            }
            return $errors;
        }
    }

}

$mform = new deanmessage_form(null, array('id' => $course->id), PARAM_RAW);

$urltogo = new moodle_url('/course/view.php', array('id' => $course->id)); // Navigate away from the form

if ($mform->is_cancelled()) {
    purge_all_caches();
    redirect($urltogo);
} else if ($fromform = $mform->get_data()) {
    // Process form data
    $draftareaid = file_get_submitted_draft_itemid('message');
    $content = file_prepare_draft_area($draftareaid, $PAGE->context->id, 'block_program_forums', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        // 'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => false), $fromform->message);

    $format = editors_get_preferred_format();

    $fs = get_file_storage();

    $content = file_save_draft_area_files($fromform->message['itemid'], $PAGE->context->id, 'block_program_forums', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        // 'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => false), $fromform->message['text']);

    $record = new stdClass();
    $record->id = '';
    $record->subject = $fromform->subject;
    $record->message = $fromform->message['text'];
    $record->timecreated = time();
    $record->user_who_send = $fromform->userid;
    $record->send_to = 'All';

    $save = $DB->insert_record('dean_messages', $record);

    file_save_draft_area_files($fromform->files, $context->id, 'block_program_forums', 'message', $save, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 50));

    // Mail to the students here
    $students = fetch_all_students();
    $message = $DB->get_record('dean_messages', ['id' => $save]);
    $br = html_writer::empty_tag('br');
    $out = array();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
        $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
        $out[] = html_writer::link($url, $filename) . $br;
    }
    $post .= $message->message;
    $post .= '<span style="font-weight: bold;">' . implode($out) . '</span>';

    foreach ($students as $student) {
        // Set content-type header for sending HTML email

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // Additional headers
        $headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
        mail($student->email, $message->subject, $post, $headers);
    }

    // SEND A COPY OF THE EMAIL to the sender
    $admin = core_user::get_support_user();
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    // Additional headers
    $headers .= 'From: ' . fullname($admin) . '<' . $admin->email . '>' . "\r\n";
    $headers .= 'Cc: raymondm@regenesys.co.za' . "\r\n";
    mail($USER->email, $message->subject, $post, $headers);
    // Redirect to the messages page
    redirect(new moodle_url('/blocks/program_forums/deanmessages.php', ['id' => $course->id, 'userid' => $USER->id]));
}
echo $OUTPUT->header();

if ($userid == 49944 || $userid == 1088) {
    $mform->display();
} else {
    // echo 'Invalid access detected, Only the Dean can use this page.';
    throw new Exception('Invalid access detected, Only the Dean can use this page.');
}



echo $OUTPUT->footer();
