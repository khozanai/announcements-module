<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once '../../course/forums/courseforums.php';
$id = required_param('id', PARAM_INT);

$course = get_course($id);

$url = new moodle_url('/blocks/program_forums/index.php?id=' . $course->id);
$context = context_course::instance($course->id);
$PAGE->set_context($context);
$PAGE->set_title('New: ' . $course->shortname . ' announcement');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname . ': New course announcement'), $url);
require_login($course);

class myforum_form extends moodleform {

    public static function file_options() {
        global $COURSE, $PAGE, $CFG;

        $configmaxbytes = $CFG->maxbytes;
        // $configmaxfiles = get_config('local_mail', 'maxfiles');
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes, $configmaxbytes);
        $maxfiles = is_numeric($configmaxfiles);
        return array('accepted_types' => '*',
            'maxbytes' => $maxbytes,
            'maxfiles' => $maxfiles,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => false);
    }

    public function definition() {
        global $DB, $CFG, $cm, $course, $blockinstance;
        // $uniqueid = uniqid();
        $mform = $this->_form; // Don't forget the underscore!  592ec946c28b3 592ec964bc3d3

        $mform->addElement('header', 'general', 'Forum message');
        // Adding the standard "name" field.
        $mform->addElement('text', 'subject', 'Subject', array('size' => '64'));

        $mform->addRule('subject', null, 'required', null, 'client');
        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('editor', 'message', 'Message', null, array(
            'subdirs' => 1,
            'maxbytes' => 0,
            // 'maxfiles' => 1,
            'height' => 500, // height of the popup window
            'width' => 450,
            'changeformat' => 0,
            'context' => null,
            'noclean' => 0,
            'trusttext' => 0,
            'enable_filemanagement' => true));
        //);
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required', null, 'client');
        $mform->addElement('hidden', 'id', $course->id);

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('audio', 'video', 'document', '.xls', '.xlsx', '.ppt', '.pptx', '.pptp');
        $filemanager_options['maxbytes'] = 0;
        $filemanager_options['maxfiles'] = -1;
        $filemanager_options['mainfile'] = true;

        $mform->addElement('filemanager', 'files', 'Attach one or more files', null, $filemanager_options);

        $this->add_action_buttons();
    }

}

$mform = new myforum_form(null, array('id' => $course->id), PARAM_RAW);

$urltogo = new moodle_url('/course/view.php', array('id' => $course->id));
if ($mform->is_cancelled()) {
    purge_all_caches();
    redirect($urltogo);
} else if ($fromform = $mform->get_data()) {
    // Process form data
    $draftareaid = file_get_submitted_draft_itemid('message');
    $content = file_prepare_draft_area($draftareaid, $PAGE->context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message);

    // $format = editors_get_preferred_format();

    $fs = get_file_storage();

    $content = file_save_draft_area_files($fromform->message['itemid'], $PAGE->context->id, 'block_program_forum', 'message', $fromform->id, array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => 1,
        'height' => 500, // height of the popup window
        'width' => 450,
        'changeformat' => 0,
        'context' => null,
        'noclean' => 0,
        'trusttext' => 0,
        'enable_filemanagement' => true), $fromform->message['text']);

    $record = new stdClass();
    $record->courseid = $fromform->id;
    $record->subject = $fromform->subject;
    $record->forum_message = $fromform->message['text'];
    $record->createdby = $USER->id;
    $record->timecreated = time();
    $record->timemodified = time();
    $record->visible = 1;
    $record->sendmethod = 'course_enrolments';

    if ($save = $DB->insert_record('course_forums', $record)) {
        // svae the file(s)
        file_save_draft_area_files($fromform->files, $context->id, 'block_program_forums', 'message', $save, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 50));
        $students = $DB->get_records('role_assignments', ['contextid' => $context->id, 'roleid' => 5]);
        $admin = core_user::get_support_user();

        // fetch the file and send it in the e-mail
        $br = html_writer::empty_tag('br');
        $out = array();
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $save, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $save . '/' . $filename;
            $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
            $out[] = html_writer::link($url, $filename) . $br;
        }

        $post .= $fromform->message['text'];
        $post .= '<span style="font-weight: bold;">' . implode($out) . $br . '</span>';

        foreach ($students as $student) {
            $user = $DB->get_record('user', ['id' => $student->userid]);

            // SEND THE E-MAIL
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // Additional headers
            $headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
            mail($user->email, $fromform->subject, $post, $headers);
        }
        // SEND A COPY OF THE EMAIL to the sender
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // Additional headers
        $headers .= 'From: ' . fullname($admin) . '<' . $admin->email . '>' . "\r\n";
        $headers .= 'Cc: raymondm@regenesys.co.za' . "\r\n";
        mail($user->email, $fromform->subject, $post, $headers);

        redirect($urltogo);
    } else {
        throw new RuntimeException("Something went wrong");
    }
}
echo $OUTPUT->header();


$mform->display();


echo $OUTPUT->footer();



