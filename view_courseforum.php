<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT); // Course forum message id

$forum = $DB->get_record('course_forums', ['id' => $id], '*', MUST_EXIST);

$course = get_course($forum->courseid);
$context = context_course::instance($course->id);
$page_url = new moodle_url('/blocks/program_forums/view_courseforum.php', ['id' => $forum->id]);


require_login($course);
$PAGE->set_context($context);
$PAGE->set_url($page_url);
$PAGE->set_title('Course forum: ' . $forum->subject);
if ($course->id < 2) {
    $PAGE->navbar->add('Courses', '/');
}
$PAGE->navbar->add($course->fullname . ': Course Forums', 'courseforums.php?id=' . $course->id);
$PAGE->navbar->add($forum->subject, $page_url);
echo '<link rel="stylesheet" type="text/css" href="styles.css" />';
echo $OUTPUT->header();
$br = html_writer::empty_tag('br');
echo html_writer::start_tag('div', ['id' => 'container']);

echo $OUTPUT->heading($forum->subject);

echo $forum->forum_message;

$out = array();
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $forum->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
foreach ($files as $file) {
    $filename = $file->get_filename();
    $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $forum->id . '/' . $filename;
    $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    $out[] = html_writer::link($url, $filename) . $br . $br;
}
echo '<h3>Message Attachments</h3>';
echo '<span style="font-weight: bold">'.implode($out).'</span>';

// SHOW ALL RESPONSES

$replies = $DB->get_records_sql("SELECT * FROM {block_courseforums_replies} WHERE courseforumid = ? ORDER BY id DESC", [$forum->id]);
if ($replies) {
    echo html_writer::start_tag('div', ['id' => 'replies-div']);
    $table = new html_table();
    $table->head = ['Reply', 'User'];
    foreach ($replies as $reply) {
        $student = $DB->get_record('user', ['id' => $reply->userid]);

        $out = array();
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $reply->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $reply->id . '/' . $filename;
            $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
            $out[] = html_writer::link($url, $filename) . $br . $br;
        }
        $row = new html_table_row([
            $reply->response . implode($out),
            $OUTPUT->user_picture($student) . $br . fullname($student) . $br . date('d-F-Y H:i:s', $reply->timecreated)
        ]);
        $row->id = 'rows';
        $table->data[] = $row;
    }
    echo html_writer::table($table);
    echo html_writer::end_tag('div');
}




// BUTTON FOR ADDING RESPONSES
$button = html_writer::tag('button', 'Add a reply to this forum', ['class' => 'reponse-button']);
$button_reply_url = new moodle_url('/blocks/program_forums/courseforum_reply.php', ['forumid' => $forum->id]);
echo html_writer::link($button_reply_url, $button);



echo html_writer::end_tag('div');
echo $OUTPUT->footer();

