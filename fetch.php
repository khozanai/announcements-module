<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */
require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");

$id = required_param('id', PARAM_INT);

$course = get_course($id);

$forums = $DB->get_records('course_forums', ['courseid' => $course->id]);

$url = new moodle_url('/blocks/program_forums/fetch.php?id=' . $course->id);
$context = context_course::instance($course->id);
$PAGE->set_context($context);

$enrolledstudents = $DB->get_records_sql('select ue.userid from {user_enrolments} ue join {enrol} e '
                . 'on e.id = ue.enrolid where e.courseid = ? and e.roleid = ?', [$course->id, 5]);

$students = $DB->get_records('role_assignments', ['contextid' => $context->id, 'roleid' => 5]);
echo '<pre>';
print_r($students);

//echo '<pre>';
//print_r($enrolledstudents);

//$from_system = core_user::get_support_user();
//echo '<pre>';
//print_r($from_system);
//
//echo fullname($from_system);
/*
$forums_files = $DB->get_records('files', array(
    'contextid' => $context->id,
    'component' => 'block_program_forum'
        ));
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forum', 'message', $course->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    //  resource_print_filenotfound($slides, $cm, $course);
    die;
} else {
    $file = reset($files);
    unset($files);
}
$path = '/' . $context->id . '/block_program_forum/message/'.$course->id.'/' . $file->get_filename();
$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);

echo $fullurl;

//foreach ($forums as $forum) {
//    $content = file_rewrite_pluginfile_urls($forum->message, 'pluginfile.php', $context->id, 'block_program_forum', 'message', $course->id);
//    echo '<pre>';
//    print_r($forum);
//}

/*
$formatoptions = new stdClass;
$formatoptions->noclean = true;
$formatoptions->overflowdiv = true;
$formatoptions->context = $context;
$content = format_text($content, $slides->contentformat, $formatoptions);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $course->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    //  resource_print_filenotfound($slides, $cm, $course);
    die;
} else {
    $file = reset($files);
    unset($files);
}

$path = '/' . $context->id . '/block_program_forums/message/' . $course->id . '/' . $file->get_filename();
$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);

$slides_files = $DB->get_records('files', array(
    'contextid' => $context->id,
    'component' => 'block_program_forum'
        ));
print_r($slides_files);

foreach ($slides_files as $slides_file) {
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'mod_slides', $slides_file->filearea, $slides_file->itemid, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $path = '/' . $context->id . '/' . $slides_file->component . '/' . $slides_file->filearea . '/' . $slides_file->itemid . '/' . $filename;
        $fullurl = moodle_url::make_file_url('/pluginfile.php', $path);
        echo html_writer::link($fullurl, $filename, array('class' => 'button'));
        echo html_writer::empty_tag('br');
        echo html_writer::empty_tag('br');
        echo html_writer::empty_tag('br');
    }

    break;
}

