
require(['jquery'], function ($) {
    $(document).ready(function () {
        var list = [];
        $('input[type="checkbox"]').change(function () { // Make sure that at least assessment method is selected

            var count_checked = $('input[type="checkbox"]:checked').length;
            if (count_checked > 0) {
                $('#send_schedule_message').show();
                if ($(this).is(':checked')) {
                    list.push($(this).val() + ',');
                } else if (!$(this).is(':checked')) {
                    location.reload();
                }

            } else if (!$(this).is(':checked')) {
                location.reload();
            } else if (count_checked <= 0) {
                $('#send_schedule_message').hide();
            }

            $.post('/blocks/program_forums/schedules_students.php', {schedules: list}, function (success) {
                if (success) {
                    $('#data').html(success);
                }
            });

            // button to send the mesage to schedules
            $('#send_schedule_message').click(function () {
                var scs = [];
                var messageid = $('.messageid').attr('id');

                $('input[type="checkbox"]').each(function () {
                    if ($(this).is(':checked')) {
                        scs.push($(this).val() + ',');
                    }
                });
                $.post('/blocks/program_forums/schedules_send_message.php', {messageid: messageid, schedules: scs}, function (success) {
                    if (success) {
                        // redirect back to the message page page
                        var url = '/blocks/program_forums/view_courseforum.php?id=' + messageid;
                        $(location).attr('href', url);
                       // $('#test_data').html(success);
                    }
                });

            });
        });

    });
});