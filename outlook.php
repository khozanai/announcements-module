<?php

require_once '../../config.php';
$code = optional_param('code', 0, PARAM_RAW);
// $code = filter_input(INPUT_GET, 'code');
require_login();
$access_details = $DB->get_record('block_cf_outlook_api', ['email_address' => 'raymondm@regenesys.co.za']);
$redirect_uri = new moodle_url('/blocks/program_forums/outlook.php');
if ($code) {
    print_r($code);
    $record = new stdClass();
    $record->id = $access_details->id;
    $record->code = $code;
    $DB->update_record('block_cf_outlook_api', $record);
    redirect(new moodle_url('/blocks/program_forums/outlook.php'));
}


$data_options = '$select=subject,from,receivedDateTime,body&$top=50&$count=true$search=Dubai&$orderby=receivedDateTime DESC';
$url = 'https://graph.microsoft.com/v1.0/me/mailfolders/inbox/messages?';

$PAGE->set_title('Emails');
$PAGE->navbar->add('Courses', '/');
echo $OUTPUT->header();

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
// curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPGET, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    // 'Content-Type: application/x-www-form-urlencoded',
    'Accept: application/json',
    'Authorization: Bearer ' . $access_details->access_token,
    'X-AnchorMailbox: raymondm@regenesys.co.za'
));
curl_setopt($ch, CURLOPT_GETFIELDS, $data_options);
// curl_setopt($ch, CURLOPT_POSTFIELDS, $data_options);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);

$outputs = json_decode($result);

if ($outputs->error->code == 'InvalidAuthenticationToken') {
    var_dump($outputs);
    /**
     * GET THE TOKEN AGAIN
     */
    $post_data = "client_id=" . $access_details->app_id . "&"
            . "client_secret=" . $access_details->app_secret . "&"
            . "code=" . $access_details->code . "&grant_type=authorization_code&redirect_uri=https://dev.regenesys.net/blocks/program_forums/outlook.php";
    $url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    $results = json_decode($result);
    if ($results->error == 'invalid_grant') { // need to generate another code
        // AADSTS70002: Error validating credentials. AADSTS70008: The provided authorization code or refresh token is expired. Send a new interactive authorization request for this user and resource
        $request_url = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize';
        $options = 'client_id=' . $access_details->app_id . 'redirect_uri=' . $redirect_uri . '&response_type=code&scope=Mail.Read Mail.Send';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        print_r($result);
    }

    $record = new stdClass();
    $record->id = $access_details->id;
    $record->access_token = $results->access_token;
    $record->scope = $results->scope;
    $record->token_type = $results->token_type;

    // $DB->update_record('block_cf_outlook_api', $record);
    // redirect(new moodle_url('/blocks/program_forums/outlook.php'));
} else {

    /**
     *  DISPLAY THE E-MAILS
     */
    // print_r($outputs);
    $table = new html_table();
    $table->head = ['Subject', 'Sender', 'Email', 'Date'];

    foreach ($outputs as $output) {
        foreach ($output as $value) {
            $link = new moodle_url('/blocks/program_forums/view_email.php', [
                'id' => $value->id
            ]);
            $row = new html_table_row([
                html_writer::link($value->webLink, $value->subject),
                $value->from->emailAddress->name,
                $value->from->emailAddress->address,
                $value->receivedDateTime
            ]);
            $table->data[] = $row;
        }
    }
    echo html_writer::table($table);
}
echo $OUTPUT->footer();













/*



  $code = 'OAQABAAIAAABHh4kmS_aKT5XrjzxRAtHz-BqD0bN01YGgmsHHkxU-6aF_38L1k9buuWR3kqZGJX31iOju'
  . 'dPwDeCeBhyUdkCY2XyP0IBeu0TxS3avUtRLQPuFZT-9ZcDE-As8olEJu7VoG44B1XXBSYqi7jCRJ5ljj6hr9FTwjqt-zyLnY31OBoTTaV'
  . 'XJRp9bizzzyZYaryJUAukIQ-c8rAfL9l-kZ-wFhWUFYwpQthkaJQEon4pU_ImS9_YKw0kcATgDAS4Oo_lr06-DI3j_-CzCFhT_blA6xsx'
  . 'Yy5LrgxpATlU_hTyXnY2MPvl_ehaqGsWNHv83XSrv_Fhg8QXLxUAGs-2yUISrBHZiJf-j3wKdX5fmvYj7b41x3gbvghJ8Ky3AuNaG'
  . 'uUZpOEkVEgHEZuTnWiT7fvFlSWRsqZ8Cp-YWU742ihLn94vLf2EvWux-9czqzOHylR2I2yqp_ADHON-uJFmQsDmMSJs92U0HX8ZV42f55ZSUPHGhYrb58LjKusuVn9km-US'
  . 'LUcCeDdul0dbQD4RUI_I7kzZyPWNKGJRhA0y5k6_ZEbTYVdqkajcGh9sLTIA9iSmtwF-ENHhTYO1jNUvlyURJnC-Un85a1GDxpzfS9o1t8JBVQAwt7JJwz5_NoNrSQdzEgAA';

$appID = 'f3a26287-bd1a-4590-be1a-60f425eee01b';
$secret = 'nxyfBDHG735-*njdFAT60}|';
$access_token = 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFCSGg0a21TX2FLVDVYcmp6eFJBdEh6Q1JtbU0tN3FKVWJja2lVNWVmdDBQdWxkczZvLTBSNXFXd1V0YThOM'
        . 'jRXTGRyRTdlYkNMYzBxbzl0WG9hYVdXejhjbXdjUGtkTlVHUnFVXzlveE1aUENBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoiejQ0d01kSHU4d0tzdW1yYmZhSzk4cXh'
        . 'zNVlJIiwia2lkIjoiejQ0d01kSHU4d0tzdW1yYmZhSzk4cXhzNVlJIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb'
        . '3dzLm5ldC8xODU4ZTg5MS0xNmRkLTQ4MDItYTA5OC02ODBhMTcyNzUxMjEvIiwiaWF0IjoxNTE3NDc3MTM5LCJuYmYiOjE1MTc0NzcxMzksImV4cCI6MTUxNzQ4MTAzOSwiYWNyIjoi'
        . 'MSIsImFpbyI6IkFTUUEyLzhHQUFBQXYzbXdlRFMrcWNVZld6VEhLZ0s1eUNJWXF0cWFwdXNKKzZUaDRDUHVXcms9IiwiYW1yIjpbInB3ZCJdLCJhcHBfZGlzcGxheW5h'
        . 'bWUiOiJNeUFwcCIsImFwcGlkIjoiZjNhMjYyODctYmQxYS00NTkwLWJlMWEtNjBmNDI1ZWVlMDFiIiwiYXBwaWRhY3IiOiIxIiwiZmFtaWx5X25hbWUiOiJNbGFtYm8iLCJnaXZl'
        . 'bl9uYW1lIjoiUmF5bW9uZCIsImlwYWRkciI6IjE1NC4xMTcuMTg0LjE5NCIsIm5hbWUiOiJSYXltb25kIE1sYW1ibyIsIm9pZCI6IjMyYWM0NGRjLTdhZmItN'
        . 'DMxMy1hODczLTY5NWQ4ZDJhZDk2OCIsInBsYXRmIjoiNSIsInB1aWQiOiIxMDAzM0ZGRjk1MDI1MzZEIiwic2NwIjoiTWFpbC5SZWFkIiwic2lnbmluX3N0YXRlIjpbIm'
        . 'ttc2kiXSwic3ViIjoibXdjU3M5YWFSQ1hlUVNPZjRzbktfNTlLNndvb0VHWnQ4d1Nac0tIWmNSZyIsInRpZCI6IjE4NThlODkxLTE2ZGQtNDgwM'
        . 'i1hMDk4LTY4MGExNzI3NTEyMSIsInVuaXF1ZV9uYW1lIjoiUmF5bW9uZE1AcmVnZW5lc3lzLmNvLnphIiwidXBuIjoiUmF5bW9uZE1AcmVnZW5lc3lzLmNvLnphIiw'
        . 'idXRpIjoiazdfakF0TC0yMFdkaThnMlVBNFVBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiNjJlOTAzOTQtNjlmNS00MjM3LTkxOTAtMDEyMTc3MTQ1ZTEwIl19.EzLGw1V-_ZpZC'
        . '_EZuX_AIAvQXXlDIfM3RkWhqET3MHeEOyBxEGzW_42KCkADUFaZalL5BO6DCbNUqyoRc2UvjMeD72_zr1cg_XBTPwWzp0ngWmLJIYhzKaN-vOPeLZAVZ'
        . '_dSJs4ZL9zWxxEt9wWdpNlwbP0jcbSr9GWqz9FAT6Xs_uyc90l9k4WsDq-1ja_AaTzKtwyq5n07sTNSRbh-yYqERr6X7MRRWOKKCmzP0Kv6azBAlOdjCmm0e'
        . 'UoeZ1KsyMBmd5Kd8nuoWcmbYh9oFmBOv8DL2El8RaCLy5ikusrupnftHHvC5UcY_19RvvOm18-6aFy-SEHArJkzuCIX0g';



$post_data = "client_id=" . $appID . "&"
        . "client_secret=" . $secret . "&"
        . "code=" . $code . "&grant_type=authorization_code&redirect_uri=https://dev.regenesys.net/blocks/program_forums/outlook.php";
$url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
print_r($result);


