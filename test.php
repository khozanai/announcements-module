<?php

require_once '../../config.php';

require_once 'lib.php';

echo $OUTPUT->header();

$br = html_writer::empty_tag('br');

$url = 'https://prodservices.regenesys.net/api/Moodle/GetScheduleDetailDetailsByQualCodeMoodleIdAndAcademicYear?'
        . 'qualCode=mba-9&mooodleID=66&academicYear=2018&UserName=moodleadmin&Password=a9m1n';

$results = json_decode(file_get_contents($url));
// var_dump($results);
$table = new html_table();
$table->head = ['Schedule', 'Firstname', 'Lastname', 'E-mail', 'Reggie #'];
foreach ($results as $result) {

    $st_url = 'https://prodservices.regenesys.net/api/Moodle/GetStudentDetailsByScheduleID?Schedule_Id=' . $result->Qualification_Schedule_Id . '&UserName=moodleadmin&Password=a9m1n';
    $outputs = json_decode(file_get_contents($st_url));
    foreach ($outputs as $output) {

        $row = new html_table_row([
            $result->Schedule_Name, $output->FirstName, $output->LastName, $output->Email_Address, $output->Student_Code
        ]);
        $table->data[] = $row;
    }
    break;
}

echo html_writer::table($table);
echo $OUTPUT->footer();
