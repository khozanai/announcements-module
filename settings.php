<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$settings->add(new admin_setting_heading(
        'headerconfig', get_string('headerconfig', 'block_program_forums'), get_string('descconfig', 'block_program_forums')
));

$settings->add(new admin_setting_configcheckbox(
        'block_program_forums/Allow_HTML', get_string('labelallowhtml', 'block_program_forums'), get_string('descallowhtml', 'block_program_forums'), '0'
));
