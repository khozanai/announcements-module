<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT); // The Course ID
$messageid = required_param('messageid', PARAM_INT); // The message's ID

$course = get_course($id);
$message = $DB->get_record('dean_messages', ['id' => $messageid]);
$url = new moodle_url('/blocks/program_forums/deanmessages.php', ['id' => $course->id, 'messageid' => $message->id]);

$context = context_course::instance($course->id);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_title('Dean\'s messages');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname), new moodle_url('/'));
$PAGE->navbar->add(('Dean\'s messages'), new moodle_url('/blocks/program_forums/deanmessages.php', ['id' => $course->id, 'userid' => $USER->id]));
$PAGE->navbar->add(('Messages from the Dean'), $url);

echo $OUTPUT->header();

$br = html_writer::empty_tag('br');

echo $OUTPUT->heading($message->subject);

echo $message->message . $br;
// output any files
$out = array();
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
foreach ($files as $file) {
    $filename = $file->get_filename();
    $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
    $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    $out[] = html_writer::link($url, $filename) . $br . $br;
}
echo implode($out);

echo $OUTPUT->footer();
