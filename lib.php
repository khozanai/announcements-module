<?php

/**
 * Description of courseforums
 *
 * @author Raymond Mlambo 2017
 * 
 * Create a course forum instance, save it and e-mail to specified students
 */

/**
 * Get all enrolled students
 * @global stdClass $DB
 * @return type
 */
function fetch_all_students() {
    global $DB;

    $query = 'SELECT DISTINCT(u.id), u.firstname, u.lastname, u.email FROM {user_enrolments} ue JOIN {enrol} e ON e.id = ue.enrolid '
            . 'JOIN {course} c ON c.id = e.courseid JOIN {user} u ON u.id = ue.userid WHERE '
            . 'c.category > ? AND e.courseid != ? AND e.roleid = ?';
    return $students = $DB->get_records_sql($query, [2, 54, 5]);
}

/**
 * handles files in this plugin
 * @global type $CFG
 * @global stdClass $DB
 * @param type $course
 * @param type $cm
 * @param type $context
 * @param type $filearea
 * @param array $args
 * @param type $forcedownload
 * @param array $options
 * @return boolean
 */
function block_program_forums_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options = array()) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");
    // Make sure the filearea is one of those used by the plugin.
    if ($filearea !== 'message') {
        return false;
    }

    require_login($course);

    $itemid = array_shift($args); // The first item in the $args array.
    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.
    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/' . implode('/', $args) . '/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'block_program_forums', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering. 
    // From Moodle 2.3, use send_stored_file instead.
    send_file($file, $filename, 0, $forcedownload, $options);
}

/**
 * Return an array of all enrolled userids
 * @global stdClass $DB
 * @param int $categoryid
 * @return array of enrolled userids
 */
function get_programme_students($categoryid) {
    global $DB;
    $programme_students = $DB->get_records_sql('SELECT distinct(ue.userid) FROM {enrol} e JOIN {user_enrolments} ue '
            . 'ON ue.enrolid = e.id JOIN {course} c ON c.id = e.courseid WHERE c.category = ?', [$categoryid]);
    return $programme_students;
}

/**
 * Send the message to the students
 * @global stdClass $DB
 * @global stdClass $USER
 * @param int $categoryid
 * @param varchar $subject
 * @param text $message
 */
function send_email_to_students($categoryid, $subject, $message) {
    global $DB, $USER;
    $students = get_programme_students($categoryid);
    foreach ($students as $student) {
        $user = $DB->get_record('user', ['id' => $student->userid]);
        // Set content-type header for sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // Additional headers
        $headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
        // $headers .= 'Cc: shivab@regenesys.co.za' . "\r\n";
        mail($user->email, $subject, $message, $headers);
    }

    // SEND A COPY OF THE EMAIL to the sender
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    // Additional headers
    $headers .= 'From: ' . fullname($USER) . '<' . $USER->email . '>' . "\r\n";
    $headers .= 'Cc: raymondm@regenesys.co.za' . "\r\n";
    mail($USER->email, $subject, $message, $headers);
}
