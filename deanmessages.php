<?php

require_once '../../config.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);

$course = get_course($id);

$url = new moodle_url('/blocks/program_forums/deanmessages.php', ['id' => $course->id, 'userid' => $USER->id]);
$context = context_course::instance($course->id);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_title('Dean\'s messages');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname), new moodle_url('/'));
$PAGE->navbar->add(('Messages from the Dean'), $url);

echo $OUTPUT->header();

$messages = $DB->get_records_sql("SELECT id, subject, message, timecreated FROM {dean_messages} ORDER BY id DESC");
$br = html_writer::empty_tag('br');
$table = new html_table();
$table->head = ['Subject', 'Date'];

foreach ($messages as $message) {
    $view_url = new moodle_url('/blocks/program_forums/view_deanmessages.php', ['id' => $course->id, 'messageid' => $message->id]);
    $row = new html_table_row(array(
        '<a href="' . $view_url . '">' . $message->subject . '</a>',
        date('D d M Y H:i:s', $message->timecreated)
    ));
    $row->attributes['class'] = '';
    $table->data[] = $row;
}
echo html_writer::table($table);
echo $OUTPUT->footer();

/* $out = array();
      $fs = get_file_storage();
      $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $message->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
      foreach ($files as $file) {
      $filename = $file->get_filename();
      $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $message->id . '/' . $filename;
      $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
      $out[] = html_writer::link($url, $filename) . $br . $br;
      }
      echo implode($out);
     * 
     */