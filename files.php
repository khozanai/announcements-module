<?php

require_once '../../config.php';
require_once 'lib.php';
require_once("$CFG->libdir/moodlelib.php");
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/resourcelib.php");
$id = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);

$course = get_course($id);

$url = new moodle_url('/blocks/program_forums/files.php', ['id' => $course->id, 'userid' => $USER->id]);
$context = context_course::instance($course->id);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_title('Dean\'s message');
$PAGE->set_pagelayout('course');
$PAGE->set_url($url);
$PAGE->navbar->add(($course->shortname), new moodle_url('/'));
$PAGE->navbar->add(('New Message From The Dean'), $url);

echo $OUTPUT->header();



echo $OUTPUT->footer();



